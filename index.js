const express = require('express');

const dotenv = require('dotenv');

const mongoose = require('mongoose');

const app = express();

const port = 3001;


//Initialize dotenv
dotenv.config();

//MONGOOSE CONNECTION
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PW}@zuitt-bootcamp.uzz5mzo.mongodb.net/?retryWrites=true&w=majority`, 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//Initialize Mongoose connection to MongoDB
let db = mongoose.connection;

//Listen to events of connection
db.on('error', console.error.bind(console, "Connection Error"));
db.on('open', () => console.log('Connected to MongoDB!'));

//CREATE SCHEMA
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
});

//CREATE MODEL
const Task = mongoose.model('Task', taskSchema);

//CREATE ROUTES
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Create single task route
app.post('/tasks', (req, res) => {
	Task.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error);
		};

		if(result != null && result.name == req.body.name){
			return res.send('Duplicate task found!');
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((error, savedTask) => {
				if(error){
					return console.error(error);
				};
				return res.status(201).send('New Task Created!');
			});
		};
	});
});

//Get all tasks
app.get('/tasks', (req, res) => {
	return Task.find({}, (error, result) => {
		if(error){
			res.send(error);
		};
		res.send(result);
	});
});

//USERS COLLECTION
//Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

//Model
const User = mongoose.model('User', userSchema);

//Register Route
app.post('/register', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result != null && result.username == req.body.username){
			return res.send('Duplicate user found!');
		} else {
			if(req.body.username !== '' && req.body.password !== ''){
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});
				newUser.save((error, savedUser) => {
					if(error){
						return res.send(error);
					};
					return res.send('New user has been registered!');
				});
			} else {
				return res.send('BOTH Username and Password must be provided.');
			};
		};
	});
});

//Get all users
app.get('/users', (req, res) => {
	return User.find({}, (error, result) => {
		if(error){
			res.send(error);
		};
		res.send(result);
	});
});

app.listen(port, () => console.log(`Server is running at port ${port}`));